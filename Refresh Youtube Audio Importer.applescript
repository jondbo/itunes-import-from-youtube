-- get path to containing directory by looking up parent path of ruby extraction script
set container_path to POSIX path of ((path to me as text) & "::")

display dialog "Re-installing Youtube Importer!"

-- setup path envvars
set command_prefix to "PATH=/usr/local/bin:$PATH; cd " & (quoted form of container_path) & " && "
log command_prefix

-- setup/rotate logfile
log "Checking for/getting latest code updates (if any)"
set git_refresh to command_prefix & "git pull origin master"
log git_refresh
set output to do shell script git_refresh
log output

log "Re-executing installation"
set call_reinstall to command_prefix & "osascript ./Install.applescript"
log call_reinstall
set output to do shell script call_reinstall
log output

display dialog "Completed re-installing!"
