require 'fileutils'

script_dir = File.expand_path(File.dirname(__FILE__))

logs_path = File.join(script_dir, 'logs')
FileUtils.mkdir_p(logs_path)

default_log_size = 5 * 1024 * 1024 * 1024
max_log_size = (ENV['max_size'] || default_log_size).to_i

log_path = File.join(logs_path, 'logfile.log')
if File.file?(log_path)
	# rotate the logs after a certain size
	if File.size(log_path) > max_log_size
		# determine file to use
		archive_log_path = nil
		archive_log_paths = (1..10).to_a.map do |index|
			log_path + ".#{index}"
		end
		archive_log_path = archive_log_paths.detect do |path|
			!File.file?(path)
		end
		unless archive_log_path
			archive_log_path = archive_log_paths.sort_by do |path|
				File.mtime(path).to_i
			end.first
		end
		FileUtils.mv log_path, archive_log_path
	end
end

puts log_path