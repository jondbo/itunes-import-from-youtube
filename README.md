# Import Audio from Youtube to iTunes

Utility for extracting the soundtrack from a Youtube video and importing it into iTunes as song(s), including metadata.  Works with playlists as well as single videos.

## Requirements

* [Mac OS X](https://www.apple.com/macos/)
	* uses system default versions of [ruby](https://www.ruby-lang.org/) and [Applescript](https://developer.apple.com/applescript)
* [Homebrew](https://brew.sh)
	* installs/updates brew-provided packages of [youtube-dl](https://rg3.github.io/youtube-dl/) and [ffmpeg](https://ffmpeg.org)

## Installation

Open the Terminal application, navigate to a directory for installing this project within, and execute the following shell commands to install the project locally, generate applications from applescript source, and install those applications to the user-level iTunes "Scripts" folder.

		git clone git@bitbucket.org:jondbo/itunes-import-from-youtube.git
		cd itunes-import-from-youtube
		osascript ./Install.applescript

## Usage

### Import Audio from Youtube

1. Launch iTunes
2. Select "Scripts" > "Import Audio from Youtube"
3. A dialog box will prompt you to open Safari and navigate to the Youtube content you wish to import
4. Once you've brought up a Youtube video (or playlist listing) page in Safari, return to iTunes and confirm the prompt to continue importing
5. A dialog box displays the the URL from the Youtube page for confirmation before importing (you may edit this to use a different Youtube URL, instead); click "OK" to continue.
6. In the background:
	* the youtube content is downloaded
	* audio & metadata are extracted and combined into one or more audio file(s)
	* audio file(s) are imported into iTunes

	...a dialog box will indicate once this importing has completed.

### Update to Latest Version

This project leverages the youtube-dl and ffmpeg utilities, which must be regularly refreshed to keep up-to-date with changes to Youtube.  To update your instalation to the latest versions of this project and its dependencies:

1. Launch iTunes
2. Select "Scripts" > "Refresh Youtube Audio Importer"
3. Dialog boxes will display to indicate progresss; confirm each prompt until the process completes.


