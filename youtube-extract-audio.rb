require 'open3'
require 'tmpdir'
require 'json'
require 'pp'
require 'logger'

this_dir = Dir.pwd

delim = "-"

arg_counter = 0
arg_prefix = '--'
arg_delimiter = '='
args = ARGV.inject(Hash.new) do |hash,arg|
	if arg.match(arg_prefix)
		pair = arg.split(arg_delimiter)
		k = pair.shift.gsub(arg_prefix,'')
		v = pair.join(arg_delimiter)
		hash[k] = v
	else
		hash[arg_counter] = arg
	end
	arg_counter += 1
	hash
end

youtube_url = args[0] || args['youtube-url'] || raise("must provide --youtube-url argument")
audio_format = args['format'] || "m4a"
extension = args['extension'] || ".#{audio_format}"

@debugging = args['debug'] == 'true'
def debugging?
	@debugging == true
end

def debug(val)
	if debugging?
		pp val
	end
	log val
end

script_dir = File.expand_path(File.dirname(__FILE__))
Dir.chdir(script_dir)

log_path = ENV['log_path'] || File.join(script_dir, 'logs', 'logfile.log')
@logger = Logger.new(log_path)
def log(val)
	@logger.info val.pretty_inspect
end

o_dir = Dir.pwd

outputs = []
tmp_dir = Dir.mktmpdir
begin
  Dir.chdir(tmp_dir) do
    
    cmd = [
    	"/usr/local/bin/youtube-dl",
    	"--write-info-json",
    	"--extract-audio",
    	"--audio-quality=0",
    	"--add-metadata",
    	"--audio-format=#{audio_format}",
    	"--prefer-ffmpeg",
    	"--verbose",
    	"--yes-playlist",
    	youtube_url,
    ]

    debug cmd
		out, err, st = Open3.capture3(*cmd)
		debug out
		debug err
		debug st

    downloads = Dir.glob("*#{extension}")
    debug downloads
    
    total_tracks = downloads.size
    downloads.each_with_index do |f,index|
      debug f
      fm = File.join(tmp_dir, f)
      debug fm
      debug File.file?(fm)
      tfb = "output_#{index}#{extension}"
      tf = File.join(tmp_dir, tfb)
      debug tf
      fj = File.join(tmp_dir, File.basename(f, extension) + '.info.json')
      debug fj
      meta = JSON.parse(File.read(fj))
      debug meta

      title, album, artist = meta['fulltitle'].to_s
      order = meta["playlist_index"].to_i
      order = order == 0 ? 1 : order

      st = meta['fulltitle'].split(delim).collect { |a| a.to_s.strip }
      if st.size == 3
        album = st[0]
        artist = st[1]
        title = st[2]
      elsif st.size == 2
        album = st[0]
        artist = st[1]
        title = st[1]
      else
        unless meta["playlist"].nil?
          album = meta["playlist"].to_s
        end
      end

      title = title.to_s.gsub(/\"/i,' ').gsub(/\s+/,' ').strip
      artist = artist.to_s.gsub(/\"/i,' ').gsub(/\s+/,' ').strip
      album = album.to_s.gsub(/\"/i,' ').gsub(/\s+/,' ').strip
      order = order.to_s.gsub(/\"/i,' ').gsub(/\s+/,' ').strip
      if total_tracks >= 2
	      order = "#{order}/#{total_tracks}"
	    end
      debug [fm, title, album, artist, order].inspect

      cmd = [
      	"/usr/local/bin/ffmpeg",
				"-loglevel",
				"verbose",
      	"-nostdin",
      	"-i",
      	fm,
      	"-metadata",
      	"title=#{title}",
      	"-metadata",
      	"author=#{artist}",
      	"-metadata",
      	"album=#{album}",
      	"-metadata",
      	"track=#{order}",
      	"-c:a",
      	"copy",
      	tf,
      	#"<",
      	#"/dev/null",
      ]
      debug cmd
	    out, err, st = Open3.capture3(*cmd)
	    debug out
	    debug err
	    debug st

			if File.file?(tf)
				outputs << tf
			else
				debug "No file created at #{tf}"
			end
    end

  end
end

# output list of files
debug outputs
puts outputs