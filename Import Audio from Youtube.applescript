-- get path to containing directory by looking up parent path of ruby extraction script
set container_path to POSIX path of ((path to me as text) & "::")
-- log container_path

-- setup PATH envvars for proper ruby execution
set ruby_invocation to "PATH=/usr/local/bin:$PATH ruby "

-- setup/rotate logfile
global log_path
set log_path to do shell script ruby_invocation & container_path & "log-setup.rb"
log "logging to: " & log_path

on log_line(this_line)
try
    log this_line
    set the log_newline to "\n"
    set open_log_file to open for access POSIX file log_path with write permission
    set this_line to ("===== " & log_newline & this_line & log_newline)
    -- log this_line
    write this_line to open_log_file starting at eof
    close access open_log_file
    return true
  on error
    log "error occurred!"
    try
      close access open_log_file
    end try
  return false
end try
end log_line

-- name of ruby-based extraction script
set extract_script to "youtube-extract-audio.rb"
-- log_line(extract_script)

set default_yt_url to ""
set yt_url to ""
repeat while yt_url is ""
	
	if application "Safari" is running then
		
		-- capture youtube URL to extract audio from
		display dialog "Please open the Youtube video to extract audio from in Safari, then click \"OK\""
		tell application "Safari"
			set test_yt_url to URL of front document
			if ((offset of "www.youtube.com" in test_yt_url) > 0) then
				set default_yt_url to test_yt_url
			end if
		end tell
		
	else
		
		set default_yt_url to ""
		
	end if
	
	set yt_input to display dialog "Youtube URL to download from:" default answer default_yt_url with icon note buttons {"Cancel", "Continue"} default button "Continue"
	
	set yt_url to text returned of yt_input
	
end repeat

display dialog "Importing audio from url \"" & yt_url & "\";

This may take a few minutes..."

-- use external script to extract youtube audio, returning a list of (temporary) audio files
with timeout of (10 * 60) seconds
	set audio_files to do shell script "log=" & (quoted form of log_path) & " " & ruby_invocation & container_path & "youtube-extract-audio.rb " & (quoted form of yt_url)
end timeout

-- hacky line-based splitting of script output
set AppleScript's text item delimiters to {return & linefeed, return, linefeed, character id 8233, character id 8232}
set audio_files to text items of audio_files
-- undo hacky splitting
set AppleScript's text item delimiters to {" "}

log_line("returned audio files: " & audio_files)

-- convert returned listing to Applescript-friendly files
repeat with audio_file in audio_files
	set audio_file's contents to (POSIX file audio_file) as Unicode text
end repeat

log_line("parsed audio files: " & audio_files)

-- ensure temporary playlist (for importing audio files to)
set import_playlist_name to "Youtube Import " & (current date)
log_line("new playlist name: " & import_playlist_name)
if application "iTunes" is not running then
	application "iTunes" launch
end if
tell application "iTunes"
	-- launch
	if not (user playlist import_playlist_name exists) then
		make new user playlist with properties {name:import_playlist_name}
	end if
end tell
-- import listed files to iTunes via the user playlist
repeat with audio_file in audio_files
	-- log_line(audio_file)
	tell application "iTunes"
		-- try
		add audio_file to user playlist import_playlist_name
		-- end try
	end tell
	tell application "System Events"
		delete file audio_file
	end tell
end repeat
-- drop temporary playlist
tell application "iTunes"
	-- if not (user playlist import_playlist_name exists) then
	delete user playlist import_playlist_name
	-- end if
end tell

display dialog "Completed importing!"
