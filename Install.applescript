tell application "Script Editor" to launch

-- compile scripts into applications and install in the user-specific iTunes Scripts folder
-- get path to containing directory
set container_path to ((path to me as text) & "::") -- get path to parent folder
log container_path

-- get path to user-specific iTunes scripts directory
set scripts_path to ((path to library folder from user domain) as text) & "iTunes:Scripts:"
log scripts_path

-- ensure that a user-specific iTunes scripts directory exists
do shell script "/bin/mkdir -p " & quoted form of (POSIX path of scripts_path)

-- define the file and application names for the varying scripts
set script_data to {"Import Audio from Youtube", "Refresh Youtube Audio Importer"}
log script_data

-- compile AppleScript applications and install in the iTunes Scripts directory
repeat with index from 1 to length of script_data
	-- extract original script name
	set script_name to item index of script_data
	log script_name

	-- setup script source & path
	set source_name to (script_name as string) & ".applescript"
	log "source name/paths:"
	log source_name
	set source_path to (container_path as string) & source_name
	log source_path

	-- setup app file name & path
	set app_name to (script_name as string) & ".app"
	log "app name/paths:"
	log app_name
	set app_path to (POSIX file (POSIX path of scripts_path) as string) & app_name
	log app_path

	-- capture script source
	set source_file to POSIX path of (source_path as string)
	log source_file
	open for access source_file
	set source to (read source_file) as text
	close access source_file

	-- replace references to container path with hardcoded path to the
	-- directory containing this template
	set replace_text to "((path to me as text) & \"::\")"
	set quoted_container_path to "\"" & (container_path as text) & "\""
	set updated_source to my findAndReplaceInText(source, replace_text, quoted_container_path)
	-- log updated_source

	-- generate application from source
	tell application "Script Editor"
		open
		set app_document to open source_file
		set contents of document 1 to updated_source
		compile document 1
		save document 1 as "application" in app_path
		close document 1 saving no
	end tell
end repeat

-- execute the dependency upgrading script
set shell_container_path to (POSIX path of container_path)
set update_script_path to shell_container_path & quoted form of "Update Youtube Audio Importer Dependencies.applescript"
log update_script_path
-- executing via shell+osacript provides stability
do shell script "/usr/bin/osascript " & update_script_path

-- relaunch iTunes so the new scripts are recognizes
if application "iTunes" is running then
	display dialog "Restarting iTunes..."
	tell application "iTunes"
		quit
	end tell
	delay 5
	tell application "iTunes"
		activate
	end tell
end if

tell application "Script Editor" to quit

-- routine from https://developer.apple.com/library/content/documentation/LanguagesUtilities/Conceptual/MacAutomationScriptingGuide/ManipulateText.html
on findAndReplaceInText(theText, theSearchString, theReplacementString)
	set AppleScript's text item delimiters to theSearchString
	set theTextItems to every text item of theText
	set AppleScript's text item delimiters to theReplacementString
	set theText to theTextItems as string
	set AppleScript's text item delimiters to ""
	return theText
end findAndReplaceInText