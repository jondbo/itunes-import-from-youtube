-- get path to containing directory by looking up parent path of ruby extraction script
set container_path to POSIX path of ((path to me as text) & "::")
-- log container_path

-- setup PATH envvars for proper ruby execution
set ruby_invocation to "PATH=/usr/local/bin:$PATH ruby "

-- setup/rotate logfile
global log_path
set log_path to do shell script ruby_invocation & container_path & "log-setup.rb"
log "logging to: " & log_path

on log_line(this_line)
try
    log this_line
    set the log_newline to "\n"
    set open_log_file to open for access POSIX file log_path with write permission
    set this_line to ("===== " & log_newline & this_line & log_newline)
    -- log this_line
    write this_line to open_log_file starting at eof
    close access open_log_file
    return true
  on error
    log "error occurred!"
    try
      close access open_log_file
    end try
  return false
end try
end log_line

-- force focus
tell me
	activate
	display dialog "Checking for / updating Youtube importing dependencies (this may take a few minutes)..."
end tell

-- setup path vars
set path_envvars to "PATH=/usr/local/bin:$PATH "

-- setup brew execution
set brew_invocation to path_envvars & "/usr/local/bin/brew "

-- check for homebrew installation
log_line("Checking for installation of homebrew package manager...")
set installed to do shell script path_envvars & "which -s brew; echo \"$?\""
log_line("installed: " & installed)
if installed is equal to "0" then
	-- update brew definitions
	log_line("Verified installation of homebrew package manager; updating to latest package definitions...")
	with timeout of (5 * 60) seconds
		set output to do shell script brew_invocation & "update"
	end timeout
	log_line("homebrew output: " & output)
	log_line("Completed updating to latest package definitions.")
else
	log_line("The Homebrew package manager is not installed; this is a requirement (go to http://brew.sh for instructions)...")
	return
end if

-- define the packages to ensure are installed
set package_names to {"youtube-dl", "ffmpeg"}
log package_names

-- compile AppleScript applications and install in the iTunes Scripts directory
repeat with index from 1 to length of package_names

	-- ensure each package is installed and up-to-date
	set package_name to item index of package_names
	log package_name

	log_line("Ensuring " & package_name & " utility is up-to-date...")

	-- check for package installation
	log_line("Checking for installation of " & package_name & " utility...")
	set installed to do shell script path_envvars & "which -s " & package_name & "; echo \"$?\""
	log_line(package_name & " installed: " & installed)
	if installed is equal to "0" then

		log_line("Verified " & package_name & " installation; checking for updates...")

		-- upgrade package to latest version
		log_line("Checking for " & package_name & " updates...")
		set outdated to do shell script brew_invocation & "outdated | grep -q " & package_name & "; echo \"$?\""
		log_line("brew outdated includes " & package_name & "?: " & outdated)

		if outdated is equal to "0" then
			log_line("Upgrading " & package_name & " utility to latest version...")
			with timeout of (5 * 60) seconds
				set output to do shell script brew_invocation & "upgrade " & package_name
			end timeout
			log_line("brew upgrade " & package_name & ": " & output)
			log_line("Cleaning up old versions of " & package_name & " utility...")
			set output to do shell script brew_invocation & "cleanup " & package_name
			log_line("brew cleanup " & package_name & ": " & output)
			log_line("Completed updating " & package_name & " utility.")
		else
			log_line("The " & package_name & " utility is up-to-date.")
		end if

	else

		log_line("The " & package_name & " utility is not installed, installing...")
		with timeout of (5 * 60) seconds
			set output to do shell script brew_invocation & "upgrade " & package_name
		end timeout
		log_line("Installed latest available " & package_name & ".")

	end if

end repeat
display dialog "Completed checking for / updating Youtube importing dependencies."
